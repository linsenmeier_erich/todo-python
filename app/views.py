from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.edit import DeleteView
from django.urls import reverse_lazy #wird genutzt, um URL r�ckw�rts (also aus einer View-Klasse heraus) aufzul�sen
from .models import Task

class TaskList(ListView):
    model = Task
    context_object_name = 'viewTasks'

class TaskDetail(DetailView):
    model = Task
    context_object_name = 'detailTask'
    #template_name = ''

class TaskCreate(CreateView):
    model = Task
    fields = '__all__' #Liste von Strings, die sichtbar sind, wenn anderer Code dieses Modul importiert (dient zur Kontrolle des Umfangs von Importen)
    success_url = reverse_lazy('tasks')

class TaskUpdate(UpdateView):
    model = Task
    fields = '__all__' #Liste von Strings, die sichtbar sind, wenn anderer Code dieses Modul importiert (dient zur Kontrolle des Umfangs von Importen)
    success_url = reverse_lazy('tasks')

class TaskDelete(DeleteView):
    model = Task
    context_object_name = 'task'
    success_url = reverse_lazy('tasks')

