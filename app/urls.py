from django.urls import path
from .views import TaskList
from .views import TaskDetail
from .views import TaskCreate
from .views import TaskUpdate
from .views import TaskDelete

urlpatterns = [
    path('', TaskList.as_view(), name='tasks'),
    path('task/<int:pk>', TaskDetail.as_view(), name='task'),
    path('taskCreate/', TaskCreate.as_view(), name='taskCreate'),
    path('taskUpdate/<int:pk>/', TaskUpdate.as_view(), name='taskUpdate'),
    path('taskDelete/<int:pk>/', TaskDelete.as_view(), name='taskDelete')
    ]