# To do list in Python with django

This is a simple web application built with Django framework that allows users to create and manage their to do list.
By Leonie, FI-C 11, OSZIMT.

# **Prerequisites**
**IMPORTANT!**
Before running the application, make sure you have the following installed:

    Python 3.x
    Django framework

You can check this by opening your command prompt and use

    python --version

to get your version of Python and

    where django-admin
    
to make sure you have django installed.

If you need to install Python:

    https://www.python.org/downloads/



If you need to install django, use pip and put this in your cmd:

    python -m pip install Django


# Getting Started

    Clone the repository: git clone <repository_url> or clone directly via GitLab
    Navigate to the project directory (cd)
    Run database migrations: python manage.py migrate
    Start the development server: python manage.py runserver
    Access the application at: http://localhost:8000

# Usage

    Create new tasks by clicking on the "Add Task" button and providing the task details.
    Edit or delete tasks using the provided options.


# Acknowledgements

    This project is inspired by various Django tutorials and resources.
    Special thanks to the Django community for their amazing work and support.
